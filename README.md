# ReactJs_bootcamp_9


## Penambahan dan Perbaikan
- Struktur File jsx ReactJs_[0.0.2]
- Perbaikan UI Home dan list Product Category ReactJs_[0.0.2]
- Penambahan Modal dan pagination category ReactJs_[0.0.3]
- Perbaikan route Navigasi ReactJs_[0.0.4]
- Penambahan Backet dan CheckOut ReactJs_[0.0.5]
- Perbaikkan pembacaan harga dalam terbilang dan cetak PDF ReactJs_[0.0.6]

## Required
- Nodejs Versi di atas v18.*
- vite  : https://vitejs.dev/guide/
- eslint : https://eslint.org/
- Tailwindcss ReactJs : https://tailwindcss.com/docs/guides/create-react-app
- Redux : https://redux-toolkit.js.org/ 
- Axio: https://axios-http.com/docs/intro
- jsPDF: hhttps://github.com/parallax/jsPDF
 
 

## License
MIT is licensed.

## Project status
Masih Dalam Pengembangan.
