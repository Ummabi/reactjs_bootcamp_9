import { configureStore } from "@reduxjs/toolkit";
import productReducer from "../store/product/slice";  
import cartReducer from "../store/cart/slice";   

export default configureStore({
    reducer: {
        product: productReducer,
        cart: cartReducer
    }
});
