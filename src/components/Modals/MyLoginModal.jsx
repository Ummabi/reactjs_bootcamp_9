import React, { useState } from 'react';

const MyLoginModal = ({ show, handleClose, handleSubmit }) => {
  const [formData, setFormData] = useState({
    // Define your form fields here
    iduser: '',
    katakunci: '',
    // Add more fields as needed
  });

  // Handle form input changes
  const handleInputChange = (e) => {
    const { iduser, value } = e.target;
    setFormData({
      ...formData,
      [iduser]: value,
    });
  };

  // Handle form submission
  const handleFormSubmit = () => {
    // Call your handleSubmit function with formData
    handleSubmit(formData);
    // Optionally, reset the form fields
    setFormData({
      iduser: '',
      katakunci: '',
      // Reset other fields as needed
    });
  };

  return (
    <div id="staticBackdrop" className={`fixed inset-0 overflow-y-auto ${show ? 'block' : 'hidden'}`}>
      <div className="flex items-center justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
        <div className="fixed inset-0 transition-opacity">
          <div className="absolute inset-0 bg-gray-500 opacity-75"></div>
        </div>
        <span className="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">&#8203;</span>
        <div className="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full">
          <div className="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
            <div className="sm:flex sm:items-start">
              <div className="mt-3 text-center sm:mt-0 sm:ml-4 sm:text-left w-full">
                <h3 className="text-lg leading-6 font-medium text-gray-900" id="staticBackdropLabel">&#128273; Login</h3>
                <div className="mt-2 w-full">
                <label htmlFor="name" className="block text-sm font-medium text-gray-700">
                    User Name
                  </label>
                  <input
                    type="text"
                    id="iduser"
                    name="iduser"
                    className="mt-1 p-2 border rounded-md w-full text-[#000000]"
                    value={formData.name}
                    onChange={handleInputChange}
                  />
                  
                  <label htmlFor="email" className="block mt-2 text-sm font-medium text-gray-700">
                    Password
                  </label>
                  <input
                    type="password"
                    id="katakunci"
                    name="katakunci"
                    className="mt-1 p-2 border rounded-md w-full text-[#000000]"
                    value={formData.email}
                    onChange={handleInputChange}
                  />
                </div>
              </div>
            </div>
          </div>
          <div className="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
            <button type="button" className="w-full inline-flex justify-center rounded-md border border-transparent shadow-sm px-4 py-2 bg-indigo-600 text-base font-medium text-white hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:ml-3 sm:w-auto sm:text-sm" data-bs-dismiss="modal">&#128275; Masuk</button>
            <button onClick={handleClose} type="button" className="mt-3 w-full inline-flex justify-center rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-base font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:mt-0 sm:w-auto sm:text-sm" data-bs-dismiss="modal">&#128272; Daftar</button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default MyLoginModal;
