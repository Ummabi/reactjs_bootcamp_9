import React, { useState } from 'react';
import "../style/style.css";
import HeaderAtas from "../Navigation/HeaderAtas";
import CheckoutPage from '../../pages/checkout';
import jsPDF from 'jspdf';
import terbilang from '../../config/terbilang';

const MyBacketModal = ({ show, handleClose, handleSubmit, product }) => {

  const handleCheckout = () => {
   
    const pdf = new jsPDF();
    const min = 1; 
    const max = 100; 
    const randomNumber = Math.floor(Math.random() * (max - min + 1)) + min;
     
    pdf.text("Invoice No. :" +randomNumber, 10, 10);
    pdf.text("Mengikuti Bootcamp " + product.title, 10, 20);
    pdf.text("Ringkasan Pembelian", 10, 30);
    pdf.text("Total Harga: " + product.price, 10, 40);
    pdf.text("Terbilang : " + terbilang(product.price), 10, 50);
    pdf.text("Metode bayar : Silahkan transfer ke Bank ABC ", 10, 80);
    pdf.text("dengan rekening 1234 44322 222 1111" , 10, 90);
     
     
    pdf.output("dataurlnewwindow");
  };

  const [formData, setFormData] = useState({
    // Define your form fields here
    name: '',
    email: '',
    // Add more fields as needed
  });

  // Handle form input changes
  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setFormData({
      ...formData,
      [name]: value,
    });
  };

  // Handle form submission
  const handleFormSubmit = () => {
    // Call your handleSubmit function with formData
    handleSubmit(formData);
    // Optionally, reset the form fields
    setFormData({
      name: '',
      email: '',
      // Reset other fields as needed
    });
  };

  return (
    <div id="staticBackdrop" className={`fixed inset-0 overflow-y-auto ${show ? 'block' : 'hidden'}`} style={{ zIndex: '9999' }}>
      <div className="flex items-center justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
        <div className="fixed inset-0 transition-opacity">
          <div className="absolute inset-0 bg-gray-500 opacity-75"></div>
        </div>
        <span className="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">&#8203;</span>
        <div className="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-[850px] sm:w-full">
          <div className="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
            <div className="sm:flex sm:items-start">
              <div className="mt-3 text-center sm:mt-0 sm:ml-4 sm:text-left w-full">
                <h3 className="text-lg leading-6 font-medium text-gray-900" id="staticBackdropLabel">&#129534; Backet </h3>
                <div className="mt-2 w-full">
                <div className="bg-[#152A46] flex flex-row rounded-[20px] ">
                  <img  src={product.image} className="w-[75px] h-[100.15px] ml-[28px] mt-[16px] mb-[16px] rounded-[20px]" alt="Product Image" />
                  
                  <div className="ml-[17px] mt-[27px]">
                  <div className="font-bold text-xl mb-2 text-[#FFCD29] ">Intensive Bootcamp</div>
                    <p className="font-bold text-base text-[#FFFFFF]">{product.title}</p>
                    <p className="text-black-700 text-base truncate w-[220px] text-[#FFFFFF]">
                      ({product.description})
                    </p>
                   <p className="font-bold text-base text-[#FFFFFF] mt-4"> Mentor </p> 
                    <table>
                      <tbody>
                        <tr>
                           
                          <td className="font-bold text-base text-[#FFFFFF]">{product.mentor}</td>
                          <td className="font-bold text-base text-[#FFFFFF] w-[80px]">&nbsp; </td>
                          <td className="font-bold text-base text-[#FFFFFF]">{product.mentor_2}</td>
                          
                        </tr>
                        <tr>
                        
                          <td  className="font-bold text-base text-[#FFFFFF]">{product.propesi_1}</td>
                          <td className="font-bold text-base text-[#FFFFFF] w-[80px]">&nbsp; </td>
                          <td  className="font-bold text-base text-[#FFFFFF]">{product.propesi_2}</td>
                        </tr>
                      </tbody>
                    </table>
                    
                    </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
          <CheckoutPage product={product}></CheckoutPage>
          <div className="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
            <button  onClick={handleCheckout} type="button" className="w-full inline-flex justify-center rounded-md border border-transparent shadow-sm px-4 py-2 bg-indigo-600 text-base font-medium text-white hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:ml-3 sm:w-auto sm:text-sm" data-bs-dismiss="modal">&#129530; Checkout</button>
            <button onClick={handleClose} type="button" className="mt-3 w-full inline-flex justify-center rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-base font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:mt-0 sm:w-auto sm:text-sm" data-bs-dismiss="modal">&#128281; Cancel</button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default MyBacketModal;
 