import React, { useState } from 'react';
import  "../../components/style/style.css";
import MyLoginModal from "../Modals/MyLoginModal";

const FooterNav = () => {
  const [isFooterNav, setIsFooterNav] = useState(true);

  return (
    <div>
      {isFooterNav ? (
       <footer className="ml-4 text-lg-start bg-light text-muted mt-4">
       <div className="footer-content">
          <section className="">
            <div className="container text-md-start mt-5">
              <div className="grid grid-cols-1 md:grid-cols-4 gap-4">
                <div className="mb-4">
                  <h6 className="text-uppercase fw-bold mb-4 txttagline">
                    <i className="fas fa-gem me-3"></i>TAGLINE edspert disini
                  </h6>
                  <p className="txtadres col-span-10">
                    Jl. Watugede No.58, Wonorejo, Sariharjo, Kec. Ngaglik, Kabupaten Sleman, Daerah Istimewa Yogyakarta 55581
                  </p>
                  
                </div>
                <div className="mb-4">
                  <h6 className="text-uppercase fw-bold mb-4 textmenuutamafooter">
                    Program
                  </h6>
                  <p>
                    <a href="#!" className="text-reset textmenufooter">Online Course</a>
                  </p>
                  <p>
                    <a href="#!" className="text-reset textmenufooter">Mini bootcamp</a>
                  </p>
                  <p>
                    <a href="#!" className="text-reset textmenufooter">bootcamp</a>
                  </p>
                </div>
                <div className="mb-4">
                  <h6 className="text-uppercase fw-bold mb-4 textmenuutamafooter">
                    Bidang ilmu
                  </h6>
                  <p>
                    <a href="#!" className="text-reset textmenufooter">Digital marketing</a>
                  </p>
                  <p>
                    <a href="#!" className="text-reset textmenufooter">Product management</a>
                  </p>
                  <p>
                    <a href="#!" className="text-reset textmenufooter">English</a>
                  </p>
                  <p>
                    <a href="#!" className="text-reset textmenufooter ">Programming</a>
                  </p>
                </div>
                <div className="mb-4">
                  <h6 className="text-uppercase fw-bold mb-4 textmenuutamafooter">
                    Tentang edspert
                  </h6>
                  <p>
                    <a href="#!" className="text-reset textmenufooter textmenufooter">Bantuan</a>
                  </p>
                  <p>
                    <a href="#!" className="text-reset textmenufooter textmenufooter">Kontak kami</a>
                  </p>
                  <p>
                    <a href="#!" className="text-reset textmenufooter textmenufooter">Media sosial</a>
                  </p>
                </div>
              </div>
            </div>
          </section>
          <div className="p-0 bg-opacity-5 copyright">
            © 2023 Copyright:
            <a className="text-reset textmenufooter fw-bold" href="https://www.edspert.id/">Edspert</a>
          </div>
          </div>
          <div className="circle"></div>
        </footer>
      ) : (
        <div>BootCampReactJS_9</div>
      )}
    </div>
  );
};

export default FooterNav;
