import React, { useState, useRef, useEffect } from 'react';
import { Outlet, useNavigate } from "react-router"; 
import "../../components/style/style.css";
import MyLoginModal from '../Modals/MyLoginModal';

const Nav = () => {
  const navigate = useNavigate();
  const [isOpen, setIsOpen] = useState(false);
  const [showProgramDropdown, setShowProgramDropdown] = useState(false);
  const [showBidangIlmuDropdown, setShowBidangIlmuDropdown] = useState(false);
  const [activeMenuItem, setActiveMenuItem] = useState(null);
 
  const [showCreateModel,setShowCreateModal] = useState(false)
 
  const toogleCreateModal =()=>{
     setShowCreateModal(!showCreateModel)
  }  

  const toggleNav = () => {
    setIsOpen(!isOpen);
  };

  const toggleProgramDropdown = () => {
    setShowProgramDropdown(!showProgramDropdown);
  };

  const toggleBidangIlmuDropdown = () => {
    setShowBidangIlmuDropdown(!showBidangIlmuDropdown);
  };

  const setMenuItemActive = (menuName) => {
    setActiveMenuItem(menuName);
  };

  const navRef = useRef(null);

  useEffect(() => {
    const closeDropdown = (e) => {
      if (navRef.current && !navRef.current.contains(e.target)) {
        setShowProgramDropdown(false);
        setShowBidangIlmuDropdown(false);
        setActiveMenuItem(null);
      }
    };

    window.addEventListener('click', closeDropdown);

    return () => {
      window.removeEventListener('click', closeDropdown);
    };
  }, []);

 

  return (
    <nav className="p-4">
      <div className="flex justify-between items-center">
        <div className="flex items-center">
          <h1 className="text-white mr-4">Edspert.id</h1>
          <button
            onClick={toggleNav}
            className="lg:hidden text-white focus:outline-none"
          >
            {isOpen ? (
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="h-6 w-6"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="M6 18L18 6M6 6l12 12"
                />
              </svg>
            ) : (
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="h-6 w-6"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="M4 6h16M4 12h16m-7 6h7"
                />
              </svg>
            )}
          </button>
        </div>

        <ul className={`lg:flex ${isOpen ? 'block' : 'hidden'} lg:space-x-4`} ref={navRef}>
          <li className={`text-white relative group px-1 py-1 ${activeMenuItem === 'Program' ? 'text-gray-500' : ''}`} onClick={() => setMenuItemActive('Program')}>
            <button onClick={toggleProgramDropdown} className="focus:outline-none relative">
              Program
              {showProgramDropdown ? (
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="h-4 w-4 ml-2 inline-block"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                >
                  <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M9 5l7 7-7 7" />
                </svg>
              ) : (
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="h-4 w-4 ml-2 inline-block"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                >
                  <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M9 5l7 7-7 7" />
                </svg>
              )}
              <ul className={`absolute ${showProgramDropdown ? 'block' : 'hidden'} bg-blue-100 mt-2 py-2 space-y-2 right-0 top-full opacity-0 group-hover:opacity-100 transition-opacity duration-300 border border-blue-300 rounded-lg w-36`}>
                <li>
                  <a href="/" className="text-black block px-4 py-2 hover:bg-blue-200 rounded">Bootcamp</a>
                </li>
                <li>
                  <a href="/" className="text-black block px-4 py-2 hover:bg-blue-200 rounded">Mini bootcamp</a>
                </li>
                <li>
                  <a href="/" className="text-black block px-4 py-2 hover:bg-blue-200 rounded">Online course</a>
                </li>
              </ul>
            </button>
          </li>
          <li className={`text-white relative group px-1 py-1 ${activeMenuItem === 'BidangIlmu' ? 'text-gray-500' : ''}`} onClick={() => setMenuItemActive('BidangIlmu')}>
            <button onClick={toggleBidangIlmuDropdown} className="focus:outline-none relative">
              Bidang Ilmu
              {showBidangIlmuDropdown ? (
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="h-4 w-4 ml-2 inline-block"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                >
                  <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M9 5l7 7-7 7" />
                </svg>
              ) : (
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="h-4 w-4 ml-2 inline-block"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                >
                  <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M9 5l7 7-7 7" />
                </svg>
              )}
              <ul className={`absolute ${showBidangIlmuDropdown ? 'block' : 'hidden'} bg-blue-100 mt-2 py-2 space-y-2 right-0 top-full opacity-0 group-hover:opacity-100 transition-opacity duration-300 border border-blue-300 rounded-lg w-40`}>
                <li>
                  <a href="/" className="text-black block px-4 py-2 hover:bg-blue-200 rounded">Digital Marketing</a>
                </li>
                <li>
                  <a href="/" className="text-black block px-4 py-2 hover:bg-blue-200 rounded">Product Management</a>
                </li>
                <li>
                  <a href="/" className="text-black block px-4 py-2 hover:bg-blue-200 rounded">Data Analysis</a>
                </li>
                <li>
                  <a href="/" className="text-black block px-4 py-2 hover:bg-blue-200 rounded">Front End Development</a>
                </li>
                <li>
                  <a href="/" className="text-black block px-4 py-2 hover:bg-blue-200 rounded">Back End Development</a>
                </li>
              </ul>
            </button>
          </li>
          <li className={`text-white  px-1 py-1 ${activeMenuItem === 'Layanan' ? 'text-gray-500' : ''}`}>
            <a href="/TentangEdspert">Tentang Edspert</a>
          </li>
          <li className="text-white">
            <a href="#" onClick={toogleCreateModal} className="bg-red-500 hover:bg-red-600 text-white px-1 py-1 rounded-full">
              Masuk/Daftar
            </a>
          </li>
        </ul>
      </div>
      <MyLoginModal show={showCreateModel} handleClose={toogleCreateModal} handleSubmit={()=>{}} ></MyLoginModal>
    </nav>
  );
};

export default Nav;
