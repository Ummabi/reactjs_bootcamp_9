import React, { useState, useRef, useEffect } from 'react';
import  "../../components/style/style.css";

const HeaderBawah = () => {
    return (
    <div className="bg-dark text-white rounded p-4 md:p-5 mb-4 flex flex-wrap">
      <div className="md:w-1/2 md:pl-4">
 
        <div className="text-container">
        <h1 className="text-4xl font-italic  "  >Jadi expert bersama edspert.id</h1>
        <p className="text-lg my-3 txtsubjom">Tingkatkan skill dan pengetahuan bersama para mentor terbaik di bidangnya, untuk siapkan karir impian anda.</p>
        </div>
      </div>
      <div className="md:w-1/2 md:pr-4">
        <img src="/assets/hader01.jpg" alt="Gambar" className="w-full md:w-auto h-auto" />
      </div>
    </div>
    );
};

export default HeaderBawah;