import React, { useState, useRef, useEffect } from 'react';
import  "../../components/style/style.css";

const HeaderBawah = () => {
    return (
    <div className="bg-dark text-white rounded p-4 md:p-5 mb-4 flex flex-wrap">
      <div className="md:w-1/2 md:pl-4">
 
        <div className="text-container">
        <h1 className="text-4xl font-italic  "  >Sudah siap bergabung?</h1>
          
        </div>
      </div>
      <div className="md:w-1/2 md:pr-4">
         
      </div>
    </div>
    );
};

export default HeaderBawah;