import React from 'react';

const TentangEdspert = () => {
  return (
    <div className=" rounded-[20px] overflow-hidden    mb-4  ml-16 w-[80%]">
      
      <div className="bg-white rounded-[20px] shadow  p-4  ml-6 mt-16 w-[90%]">
   
        <div className="mt-4 w-full">
          <h5 className="text-xl font-semibold w-full">Tentang Kami</h5>
          <p className="mt-2 text-gray-600">Kami percaya bahwa kemajuan suatu bangsa tidak lepas dari kualitas sumber daya manusianya. Oleh sebab itu Edspert hadir sebagai platform belajar skill dan pengembangan diri bersertifikat untuk meningkatkan kualitas sumber daya manusia di Indonesia agar memiliki daya saing tingkat dunia. Edspert adalah bagian dari perusahaan Widya Edutech dan juga Widya Group. </p>
          <p className="mt-2 text-gray-600">Kami menyediakan berbagai kelas live interaktif dan berbagai konten pendidikan lainnya yang diisi oleh para expert atau para ahli di bidangnya agar masyarakat dapat memperoleh kelas dan konten terbaik yang berkualitas dengan akses yang mudah.</p>
        </div>
      </div>

    </div>
  );
};

export default TentangEdspert;
