import React from 'react';
import { Outlet } from 'react-router-dom';
import Nav from "../components/Navigation/Nav";
import HeaderAtas from "../components/Navigation/HeaderAtas";
import FooterNav from "../components/Navigation/FooterNav";
 


const HomePage = () => {
     
    return (
        <>
        <header>
         <Nav/>
         <HeaderAtas/>        
        </header>
        <div className='content'>
            <Outlet />
        
        </div>
            
            <FooterNav/>
         </>
    );
}

export default HomePage;