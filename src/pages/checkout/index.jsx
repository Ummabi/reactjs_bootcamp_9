import React, { useMemo } from "react";
import terbilang from "../../config/terbilang";

const CheckoutPage = (props) => { // Terima prop product
  const product = props.product; // Dapatkan data produk dari prop

  // Menggunakan useMemo untuk menghitung total harga dengan cache
  const totalPrice = useMemo(() => {
    // Hitung total harga berdasarkan data produk
    const result = (
      parseInt(product.price, 10)  
    );
    return result;
  }, [product]);

  console.log(totalPrice);
  // Menggunakan terbilang pada totalPrice
  const totalPriceTerbilang = useMemo(() => {
    const formattedPrice = new Intl.NumberFormat("id-ID", {
      style: "currency",
      currency: "IDR",
    }).format(totalPrice);

    const numericPrice = parseFloat(
      formattedPrice.replace(/[^\d.-]/g, "").replace(",", ".")
    );

    return terbilang(totalPrice);
  }, [totalPrice]);

  
  return (
    <>
      <div className="ml-6 mr-6 mb-10">
        <h1 className="mt-4">Ringkasan Pembelian</h1>

        <p>Berikut adalah produk yang ada di dalam cart:</p>
        <table className="min-w-full">
    
          <tfoot>
            <tr>
              <td className="px-6 py-3 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                Harga
              </td>
              <td className="px-6 py-3 whitespace-no-wrap text-sm leading-5 font-medium text-gray-900">
                Rp {totalPrice}
              </td>
            </tr>
            <tr>
              <td className="px-6 py-3 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                Terbilang
              </td>
              <td className="px-6 py-3 whitespace-no-wrap text-sm leading-5 font-medium text-gray-900">
                {totalPriceTerbilang} Rupiah
              </td>
            </tr>
          </tfoot>
        </table>

        <h5>Metode bayar</h5>
        <p>Silahkan transfer ke Bank ABC dengan rekening 1234 44322 222 1111</p>
      </div>
    </>
  );
};

export default CheckoutPage;
