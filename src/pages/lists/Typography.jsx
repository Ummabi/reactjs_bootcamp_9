import classNames from "classnames";

function Typography({ size, weight, color, maxWidth, maxHeight, truncate, children, ...rest }) {
  const textStyle = classNames(
    "text-black text-[16px]",
    {
      "text-[12px]": size === "xs",
      "text-[14px]": size === "sm",
      "text-[16px]": size === "md",
      "text-[20px]": size === "lg",
      "text-[42px]": size === "xl",
    },
    {
      "font-[400]": weight === "light",
      "font-[600]": weight === "normal",
      "font-[800]": weight === "bold",
    },
    color ? color : "",
    maxWidth ? `max-w-${maxWidth}` : "",
    maxHeight ? `h-${maxHeight}` : "",
    "overflow-hidden",
    "break-words",
    "whitespace-pre-wrap"
  );

  return (
    <p
      className={truncate ? `${textStyle} truncate w-[220px]` : textStyle}
      {...rest}
    >
      {children}
    </p>
  );
}

export default Typography;
