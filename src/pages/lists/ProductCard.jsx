import React, { useState } from 'react';
import Typography from "../../pages/lists/Typography"
import "../../components/style/ProductCard.css";
import MyBacketModal from '../../components/Modals/MyBacketModal';

function ProductCard({ product }) {
  // State untuk menyimpan data produk yang dipilih
  const [selectedProduct, setSelectedProduct] = useState(null);
  const [showCreateModel, setShowCreateModal] = useState(false);

  const dateObject = new Date(product.batch);
  const options = { year: 'numeric', month: 'long' };
  const formattedDate = dateObject.toLocaleDateString(undefined, options);

  
  const toogleCreateModal = () => {
    // Pastikan product ada sebelum mengaturnya sebagai selectedProduct
    if (product) {
      setSelectedProduct(product);
      setShowCreateModal(!showCreateModel);
    }
  }

  return (
    <div className="max-w-sm rounded-[20px] overflow-hidden shadow-lg mb-4  ml-4 mr-4 mt-4 ">
      <div className="bg-[#152A46] flex flex-row rounded-t-[20px]">
        <img  src={product.image} className="w-[75px] h-[100.15px] ml-[28px] mt-[16px] mb-[16px] rounded-[20px]" alt="Product Image" />
        <div className="ml-[17px] mt-[27px]">
          <div className="font-bold text-xl mb-2 text-[#FFCD29] ">Intensive Bootcamp</div>
          <p className="font-bold text-base text-[#FFFFFF]">{product.title}</p>
          <p className="text-black-700 text-base truncate w-[220px] text-[#FFFFFF]">
            ({product.description})
          </p>
        </div>
      </div>
      <div className="bg-[#FFFFFF] flex flex-row rounded-b-[20px]">     
        <div className="ml-[40px] mt-[7px]">
          <p className="font-bold text-base text-[#000000]">{product.title}</p>
          <p className="text-gray-700 text-base truncate w-[220px] text-[#000000] mb-3"> ({product.description}) </p>
          <p className="text-gray-700 text-base text-[#000000] ">Batch &nbsp; {formattedDate}</p>
          <p className="text-gray-700 text-base truncate w-[220px] text-[#000000] ">mentor &nbsp;  {product.mentor}, {product.mentor_2}</p>
          <div className="float-right mb-2">
            <p>
              <sup><span style={{ textDecoration: 'line-through' }}>
                Rp {(product.price * 1.5) + (product.price * product.diskon / 100)}
              </span></sup>
              <span className="mx-1"> - </span>
                Rp {product.price}
            </p>
          </div>
        </div>
      </div>
      <div className="px-6 pt-4 pb-2">
        <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#Bootcamp</span>
        <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#{product.title}</span>
        <button onClick={toogleCreateModal}>
          <span className="inline-block bg-green-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">&#128269; Detail</span>
        </button>
      </div>
      <MyBacketModal show={showCreateModel} handleClose={toogleCreateModal} handleSubmit={() => {}} product={product}></MyBacketModal>
    </div>
  );
}

export default ProductCard;
