import React, { useEffect, useState } from "react";
import ProductCard from "../lists/ProductCard";
import { useDispatch, useSelector } from "react-redux";
import { getAllProducts } from "../../store/product/action";
import "../../components/style/style.css";

const ListPage = () => {
  const { entities, loading } = useSelector((state) => state.product);
  
  //mengatur pagination
  const [currentPage, setCurrentPage] = useState(1);
  const productsPerPage = 6;
  //end pegination

  const dispatch = useDispatch();

  const fetchProduct = async () => {
    dispatch(getAllProducts());
  };

  /*
  /menngunakan  state biasa
  const fetchProduct = async () => {
    try {
      const response = await httpService.get("/katalog");
      setList(response.data);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };
  */


  useEffect(() => {
    fetchProduct();
  }, []);

  const indexOfLastProduct = currentPage * productsPerPage;
  const indexOfFirstProduct = indexOfLastProduct - productsPerPage;
  const currentProducts = entities.slice(indexOfFirstProduct, indexOfLastProduct);

  const handlePageChange = (newPage) => {
    setCurrentPage(newPage);
  };

  const prev = () => {
    if (currentPage > 1) {
      handlePageChange(currentPage - 1);
    }
  };

  const next = () => {
    if (currentProducts.length >= productsPerPage) {
      handlePageChange(currentPage + 1);
    }
  };

  return (
    <div className="grid-container">
      {loading ? (
        <>
        <div className="inline-block relative">
          <div className="animate-spin rounded-full h-5 w-5 border-t-2 border-b-2 border-gray-900"></div>
        </div>
        <span className="ml-1">Loading...</span>
        </>
      ) : (
        currentProducts.map((product) => (
          <ProductCard key={product.id} product={product} />
        ))
      )}

      <br/>
      <div className="pagination">
      <div className="pagination-buttons">
        <button
          className={`${
            currentPage === 1 ? "opacity-50 cursor-not-allowed" : ""
          }`}
          onClick={prev}
          disabled={currentPage === 1}
        >
          <span className="flex items-center gap-2">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-4 w-4"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth={2}
                d="M10 19l-7-7m0 0l7-7m-7 7h18"
              />
            </svg>
            Previous&nbsp;
          </span>
        </button>

        {Array.from({ length: Math.ceil(entities.length / productsPerPage) }).map(
          (item, index) => (
            <button
              key={index}
              className={`${
                currentPage === index + 1 ? "bg-blue-500 text-white" : ""
              }`}
              onClick={() => handlePageChange(index + 1)}
            >
              {index + 1}
            </button>
          )
        )}

        <button
          className={`${
            currentProducts.length < productsPerPage
              ? "opacity-50 cursor-not-allowed"
              : ""
          }`}
          onClick={next}
          disabled={currentProducts.length < productsPerPage}
        >
          <span className="flex items-center gap-2">
            Next
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-4 w-4"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth={2}
                d="M14 5l7 7m0 0l-7 7m7-7H3"
              />
            </svg>
          </span>
        </button>
      </div>
    </div>
    </div>
  );
}

export default ListPage;
