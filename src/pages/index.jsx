import { RouterProvider, createBrowserRouter } from "react-router-dom";
import HomePage from "../pages/HomePage";
import ListPage from "../pages/lists/index";
import DetailPage from "../pages/details/index";
import CheckoutPage from "../pages/checkout/index";
import TentangEdspert from  "../pages/links/tentang/TentangEdspert";
import { Provider } from "react-redux";
import store from "../store";

const route = createBrowserRouter([
  {
    path: "/",
    element: <HomePage />,
    children: [
      {
        index: true,
        element: <ListPage />,
      },
      {
        path: "TentangEdspert",  
        element: <TentangEdspert />,
      },
      {
        path: "detail/:id",
        element: <DetailPage />,
      },
      {
        path: "checkout",
        element: <CheckoutPage />,
      },
    ],
  },
])

const IndexHome = () => {
    return (
      <Provider store={store}>
        <RouterProvider router={route} />
      </Provider>
    );
  }
  
  export default IndexHome;

